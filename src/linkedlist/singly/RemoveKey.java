package linkedlist.singly;


public class RemoveKey {
    private ListNode head;
    private static class ListNode{
        private int data;
        private ListNode next;
        
        public ListNode(int data){
            this.data=data;
            this.next=null;
        }
    }

    public static void main(String[] args) {
        RemoveKey removeKey=new RemoveKey();
        removeKey.head=new ListNode(2);
        ListNode second=new ListNode(3);
        ListNode third=new ListNode(4);
        ListNode fourth=new ListNode(6);
        ListNode fifth=new ListNode(7);
        ListNode sixth=new ListNode(8);

        removeKey.head.next=second;
        second.next=third;
        third.next=fourth;
        fourth.next=fifth;
        fifth.next=sixth;

        removeKey.display();
        removeKey.remove(3);
        removeKey.display();
    }

    private void remove(int value) {
        ListNode current=head;
        ListNode temp=null;
        
        while (current!=null && current.data!=value){
            temp=current;
            current=current.next;
        }
        if (current.next==null){
            return;
        }else {
            temp.next=current.next;
        }

    }

    private void display() {
        ListNode current=head;
        System.out.println("start print");
        while (current!=null){
            System.out.println(current.data);
            current=current.next;
        }
    }
}
