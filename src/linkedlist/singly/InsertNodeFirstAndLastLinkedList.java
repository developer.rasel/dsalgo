package linkedlist.singly;

public class InsertNodeFirstAndLastLinkedList {
    ListNode head;
    private static class ListNode{
        private int data;
        private ListNode next;

        public ListNode(int data){
            this.data=data;
            this.next=null;
        }
    }

    public static void main(String[] args) {
        InsertNodeFirstAndLastLinkedList insertNodeFirstAndLastLinkedList =new InsertNodeFirstAndLastLinkedList();
        insertNodeFirstAndLastLinkedList.head=new ListNode(22);
        ListNode second=new ListNode(32);
        ListNode third=new ListNode(42);
        ListNode fourth=new ListNode(52);

        insertNodeFirstAndLastLinkedList.head.next=second;
        second.next=third;
        third.next=fourth;

        insertNodeFirstAndLastLinkedList.display("create new node");
        insertNodeFirstAndLastLinkedList.insertNewNodeFirstPosition();
        insertNodeFirstAndLastLinkedList.insertNewNodeLastPosition();
    }

    private void insertNewNodeLastPosition() {
        ListNode newNode=new ListNode(62);

        if (head==null){
            head=newNode;
            display("insert only node in last position");
            return;
        }

        ListNode current=head;
        while (current.next!=null){
            current=current.next;
        }
        current.next=newNode;
        display("insert new node in last position");
    }

    private void insertNewNodeFirstPosition() {
        ListNode newNode=new ListNode(12);
        newNode.next=head;
        head=newNode;
        display("insert new node in first position");
    }

    private void display(String message) {
        ListNode current=head;
        System.out.println(message);
        while (current!=null){
            System.out.print(current.data+", ");
            current=current.next;
        }
        System.out.println();
    }
}
