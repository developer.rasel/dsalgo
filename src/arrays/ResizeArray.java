package arrays;

public class ResizeArray {
    public static void main(String[] args) {
        Integer[] numArray={4,2,7};

        Integer[] newArray=new Integer[6];

        for (int i=0;i<numArray.length;i++){
            newArray[i]=numArray[i];
        }

        ArrayUtils arrayUtils=new ArrayUtils();
        arrayUtils.printArray(newArray,"resize array");
    }
}
